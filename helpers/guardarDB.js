const fs =require('fs');

const archivo ='./db/data.json';

const guardarDB = (data) => {
  // console.log(data);
  fs.writeFileSync(archivo ,JSON.stringify(data));
}

const getDB = () =>{
  if (!fs.existsSync(archivo)) {
    return null;
  }

  //leer el archivo :
  fs
  const info = fs.readFileSync(archivo, {encoding: 'utf-8'});

  const data  = JSON.parse(info);
  // console.log(data);
  return data;
}

module.exports ={
  guardarDB,
  getDB
}